package se.cag.cucumberdemo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class MyStepDefs_EN {

    private RpnCalculator rpnCalculator;

    @Given("^a calculator I just turned on$")
    public void a_calculator_I_just_turned_on() throws Throwable {
        rpnCalculator = new RpnCalculator();
        Assert.assertNotNull(rpnCalculator);
    }

    @When("^I add (\\d+) and (\\d+)$")
    public void I_add_and(double arg1, double arg2) throws Throwable {
        rpnCalculator.push(arg1);
        rpnCalculator.push(arg2);
        rpnCalculator.push("+");
    }

    @When("^I multiply (\\d+) and (\\d+)$")
    public void I_multiply_and(double arg1, double arg2) throws Throwable {
        rpnCalculator.push(arg1);
        rpnCalculator.push(arg2);
        rpnCalculator.push("*");
    }

    @Then("^the result is (\\d+)$")
    public void the_result_is(double arg1) throws Throwable {
        Assert.assertEquals(arg1, rpnCalculator.value());
    }

    @And("^subtract (\\d+)$")
    public void subtract(double arg1) throws Throwable {
        rpnCalculator.push(arg1);
        rpnCalculator.push("-");
    }

    @When("^I subtract (\\d+) and (\\d+)$")
    public void I_subtract_and(double arg1, double arg2) throws Throwable {
        rpnCalculator.push(arg1);
        rpnCalculator.push(arg2);
        rpnCalculator.push("-");
    }

    @Then("^the result is (-\\d+)$")
    public void the_result_is_(double arg1) throws Throwable {
        Assert.assertEquals(arg1, rpnCalculator.value());
    }

}
