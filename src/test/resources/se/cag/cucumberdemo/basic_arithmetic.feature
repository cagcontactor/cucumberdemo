Feature: Basic Arithmetic

  Background: A Calculator
    Given a calculator I just turned on

  Scenario: Addition
  # Try to change one of the values below to provoke a failure
    When I add 5 and 5
    Then the result is 10

  Scenario: Another Addition
  # Try to change one of the values below to provoke a failure
    When I add 4 and 7
    Then the result is 11

  Scenario: A multiply
  # Try to change one of the values below to provoke a failure
    When I multiply 4 and 7
    And subtract 5
    Then the result is 23

  Scenario: A subtraction
    When I subtract 4 and 7
    Then the result is -3